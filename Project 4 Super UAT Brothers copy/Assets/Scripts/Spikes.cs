﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour
{
    // A vector for the player's position.
    Vector3 playerPosition;


    // Start is called before the first frame update
    void Start()
    {
        // The player position equals the transform position of the game object that has the tag Player.
        playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // A OnCollisionEnter2D function.
    void OnCollisionEnter2D(Collision2D col)
    {
        // An if statement for whenever the spikes collider collides with the player's collider.
        if (col.gameObject.tag == "Player")
        {
            // If the player toouches the spikes they will return back to the original spawn position within the level.
            col.gameObject.gameObject.transform.position = playerPosition;
        }
    }
}
