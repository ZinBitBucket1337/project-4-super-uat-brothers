﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // Attchment to a TargetJoint2D with a viariable represented by tj.
    TargetJoint2D tj;
    // Attachment to the player game object
    GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        // tj is equal to the TargetJoind2D component.
        tj = GetComponent<TargetJoint2D>();
        // The player is equal to the game object with the tag player.
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        // The TargetJoind2D will position itself to the player's position.
        tj.target = (Vector2)player.transform.position;
    }
}
