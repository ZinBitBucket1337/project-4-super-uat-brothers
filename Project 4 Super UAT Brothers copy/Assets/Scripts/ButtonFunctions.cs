﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonFunctions : MonoBehaviour
{
    // A public function that allow's a button to quit the application if clicked.
    public void Quit()
    {
        Application.Quit();
    }

    // A public function that transitions the game to the main menu to the first level.
    public void LoadLevel(int level)
    {
        SceneManager.LoadScene(level);
        PlayerPrefs.SetInt("Score", 0);
    }
}
