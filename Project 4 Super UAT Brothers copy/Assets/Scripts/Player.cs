﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // The attachment to the animator.
    Animator anim;
    // The attachment to the Rigidbody2D.
    Rigidbody2D rb;
    // The variable in charge of speed (DESIGNER CAN MODIFY).
    public float speed;
    // The variable in charge of the jump force (DESIGNER CAN MODIFY).
    public float jumpForce;
    // The variable in charge of the max speed/acceleration (DESIGNER CAN MODIFY).
    public float maxSpeed;
    // A boolean variable in charge of detecting whether the player is touching the gorund 
    bool grounded;
    // The variable in charge of attaching the LayerMask to the ground detection.
    public LayerMask ground;
    // A bool varaible in charge of detecting the left side of the character.
    bool leftSide;
    // A bool variable in charge of detecting the right side of the character.
    bool rightSide;
    // A bool varaible that determines whether the player can move (Must be originally true so player can initialiy move).
    bool canMove = true;


    // Start is called before the first frame update
    void Start()
    {
        // The Rigidbody2D variable is attaching itself to the Rigidbdoy2D method.
        rb = GetComponent<Rigidbody2D>();
        // The animator variable is attaching itself to the Animtor method.
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        // A raycast for whenever the player's y-position and x-position is up it will disable the movement until it touches the ground which shoudl be null.
        grounded = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y - 1), Vector2.down, 0.1f, ground).collider != null;
        // A raycast for whenever the player touches a vertical wall he/she is able to jump from it (wall jumping for left side).
        leftSide = Physics2D.Raycast(new Vector2(transform.position.x-0.7f, transform.position.y), Vector2.down, 0.1f, ground).collider != null;
        // A raycast for whenever the player touches a vertical wall he/she is able to jump from it (wall jumping for right side).
        rightSide = Physics2D.Raycast(new Vector2(transform.position.x+0.7f, transform.position.y), Vector2.down, 0.1f, ground).collider != null;

        // An if statement for whenever the RigidBody velocity is greater than -max speed and the horizontal Input.GetAxis is less than zero and player can move.
        if (rb.velocity.x > -maxSpeed && Input.GetAxis("Horizontal") < 0 && canMove)
        {
            // The RigidBody AddForce is equal to the new vector ---> float x = -speed,  float y = 0.
            rb.AddForce(new Vector2(-speed, 0));
        }
        //  An if statement for whenever the RigidBody velocity is less than max speed and the horizontal Input.GetAxis is greater than zero and player can move.
        else if (rb.velocity.x < maxSpeed && Input.GetAxis("Horizontal") > 0 && canMove)
        {
            // The RigidBody AddForce is equal to the new vector ---> float x = speed,  float y = 0.
            rb.AddForce(new Vector2(speed, 0));
        }

        // An if statement for horizontal Input.GetAxis when it's less than zero.
        if (Input.GetAxis("Horizontal") < 0)
        {
            // The transform.localScale is equal to the new vector ---> float x = -5, float y = 5, float z = 5.
            transform.localScale = new Vector3(-5, 5, 5);
        }
        // An else if statement for horizontal Input.GetAxis when it's greater than zero.
        else if (Input.GetAxis("Horizontal") > 0)
        {
            // The transform.localScale is equal to the new vector ---> float x = 5, float y = 5, float z = 5.
            transform.localScale = new Vector3(5, 5, 5);
        }

        // The if statement for whenever the player wants to jump and is also grounded.
        if (Input.GetKeyDown(KeyCode.Space)&&grounded)
        {
            // The RigidBody velocity is equal to the new vector ---> get = rb.velocity.x, set = jumpForce.
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        }
        // The else if statement for whenever the player wants to wall jump from the left side.
        else if (Input.GetKeyDown(KeyCode.Space) && leftSide)
        {
            // The RigidBody velocity is equal to the new vector ---> float x = 8, float y = 8.
            rb.velocity = new Vector2(8, 8);
            // The start coroutine for the IEnumerator player disable movement with the float time of 0.6.
            StartCoroutine(DisableMove(0.6f));
        }
        // The else if statement for whenever the player wants to wal jump from the right side.
        else if (Input.GetKeyDown(KeyCode.Space) && rightSide)
        {
            // The RigidBody velocity is equal to the new vector ---> float x = -8, float y = 8.
            rb.velocity = new Vector2(-8, 8);
            // The start coroutine for the IEnumerator player disable movement with the float time of 0.6.
            StartCoroutine(DisableMove(0.6f));
        }


        // The animator which is set to a boolean, if the player is moving horisontally thn the walking animation is executed.
        anim.SetBool("walking", Input.GetAxis("Horizontal") != 0);

    }

    // An IEnumerator function that will disable movement during specific time frames.
    IEnumerator DisableMove(float time)
    {
        // Player cannot move beacause of the boolean value being false.
        canMove = false;
        // A yield to determine how long for disable movement.
        yield return new WaitForSeconds(time);
        // Player can now move because of the boolean value being true.
        canMove = true;
    }
}
