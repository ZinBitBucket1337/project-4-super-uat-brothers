﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gems : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // OnTriggerEnter2D function
    void OnTriggerEnter2D(Collider2D col)
    {
        // An if statement that will be processed if the gem collider collides with the player's collider.
        if (col.gameObject.tag == "Player")
        {
            // AddScore Function call.
            AddScore();
        }
    }

    // The AddScore function definiton.
    void AddScore()
    {
        // Add's 100 points to the PlayerPref score.
        PlayerPrefs.SetInt("Score", PlayerPrefs.GetInt("Score") + 100);
        // Destroy the gem game object.
        Destroy(gameObject);
    }
}
