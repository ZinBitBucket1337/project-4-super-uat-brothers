﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Checkpoint : MonoBehaviour
{
    // Attachment to the game object referred to as Flag.
    GameObject Flag;
    // Start is called before the first frame update
    void Start()
    {
        // The main functionality of the checkpoint will activate once the game object triangle is interacted by the player.
        Flag = transform.Find("Triangle").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // A OnTriggerEnter2D function 
    void OnTriggerEnter2D(Collider2D col)
    {
        // An if statement for whenever the trigger interacts with the game object that has the player tag.
        if (col.gameObject.tag == "Player")
        {
            // Starting the coroutine process with the parameter of the IEnmuerator LoadNExtLevel function.
            StartCoroutine(LoadNextLevel());
        }
    }

    //
    IEnumerator LoadNextLevel()
    {
        // Once the flag's collider is touched the traingle will change color from red to green.
        Flag.GetComponent<SpriteRenderer>().color = new Color(0,.6f, 0);
        // The amount of seconds it take's for the player to transition to the next level (NEEDED FOR THE COROUTINE).
        yield return new WaitForSeconds(1);
        // The Scene Manager that will activate the next scene (Transition to the next level).
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
    }
}
