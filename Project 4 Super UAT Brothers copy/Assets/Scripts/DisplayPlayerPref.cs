﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayPlayerPref : MonoBehaviour
{
    // Attachment to text with a variable defined by t.
    Text t;
    // A public string for the PlayerPref (DESIGNER CAN MODIFY)
    public string playerPrefName;

    void Awake()
    {
        // t is equal to the text component.
        t = GetComponent<Text>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // The text will change depending on the integer .
        t.text = PlayerPrefs.GetInt(playerPrefName)+"";
    }
}
